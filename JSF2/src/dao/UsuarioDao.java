package dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import bean.UsuarioBean;
import hibernateUtil.HibernateUtil;

public class UsuarioDao {
	SessionFactory sessionFactory = HibernateUtil.buildSessionFactory();

	public boolean validarUsuario(String nome, String senha) {
		UsuarioBean usuario;

		Session session = this.sessionFactory.openSession();
		Query q = session.createQuery("from usuario where nome = :nome and senha = :senha");

		q.setString("nome", nome);
		q.setString("senha", senha);

		usuario = (UsuarioBean) q.uniqueResult();
		session.close();

		if (usuario != null)
			return true;
		else
			return false;
	}

	public Boolean salvarUsuario(String nome, int idade, String senha) {
		UsuarioBean usuario = new UsuarioBean();
		usuario.setIdade(idade);
		usuario.setNome(nome);
		usuario.setSenha(senha);

		Session session = this.sessionFactory.openSession();
		session.beginTransaction();
		session.save(usuario);
		session.getTransaction().commit();
		session.close();

		return true;
	}
}
