package resources;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "navigationController", eager = true)
@RequestScoped
public class NavigationController {
	@ManagedProperty(value = "#{param.pageId}")
	private String pageId;

	public String moveToIndex() {
		return "index.xhtml";
	}

	public String showPage() {
		if (this.pageId == null) {
			return "/index.xhtml";
		}

		if (this.pageId.equals("1")) {
			return "/index.xhtml";
		} else if (pageId.equals("3")) {
			return "/ajuda.xhtml";
		} else {
			return "/esqueciAsenha.xhtml";
		}
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}
}
